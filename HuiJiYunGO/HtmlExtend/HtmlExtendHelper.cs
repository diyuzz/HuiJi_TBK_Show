﻿using HuiJiYunGo.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace System.Web.Mvc
{
    public static class ExTextHelper
    {
        public static string ImgSrc(this HtmlHelper html, string url,string site)
        {
            if (string.IsNullOrWhiteSpace(url)) {
                return "";
            }
            else
            {
                if(url.Substring(0, 10).IndexOf("//") == -1)
                {
                    return site+""+url;
                }
                else
                {
                    return url;
                }
            }
        }
    }
}