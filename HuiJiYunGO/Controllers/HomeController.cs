﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HuiJiYunGO.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "电商好物推荐,最新优惠活动,隐藏优惠券搜索，让您得到最优惠的价格";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "期待与您的合作";

            return View();
        }
    }
}