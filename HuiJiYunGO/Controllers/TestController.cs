﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Top.Api;
using Top.Api.Request;
using Top.Api.Response;

namespace HuiJiYunGO.Controllers
{
    public class TestController : Controller
    {
        private string url = "http://gw.api.taobao.com/router/rest";
        private string appkey = "30458508";
        private string secret = "697946af51d5185616406d64990bbc14";
        // GET: Test
        public ActionResult Index()
        {
           
            return View();
        }

        public ActionResult TbkTpwdCreate() {
            //string url = "https://s.click.taobao.com/t?e=m%3D2%26s%3DoLlSAqt0cH5w4vFB6t2Z2ueEDrYVVa64Dne87AjQPk9yINtkUhsv0NcBLRgUTpxpRyzq1DyPBOW8Xm4%2FuFcvROHroAtoHl6yPesSyNKFeIjFQ3qBgQAZBVMjacS34Cq9I1qc6ZupxAs6ZgOZS7dFueV%2FpTYkWP25iufCphjQGRU89dZ%2FJv6NxrybI%2FWGa7qaUZXZHmVjgVTGJe8N%2FwNpGw%3D%3D&amp;scm=1007.15348.115058.0_28026&amp;pvid=4c710af4-ca53-4611-925b-696f3f2bc20b&amp;app_pvid=59590_11.132.118.125_593_1594450880300&amp;ptl=floorId:28026;originalFloorId:28026;pvid:4c710af4-ca53-4611-925b-696f3f2bc20b;app_pvid:59590_11.132.118.125_593_1594450880300&amp;union_lens=lensId%3AMAPI%401594450880%404c710af4-ca53-4611-925b-696f3f2bc20b_596198137886%401";
            //ITopClient client = new DefaultTopClient(url, appkey, secret);
            //TbkTpwdCreateRequest req = new TbkTpwdCreateRequest();
            //req.UserId = null;
            //req.Text ="向你推荐了";
            //req.Url = url;
            //req.Logo = "https://img.alicdn.com/bao/uploaded/i1/2975892770/O1CN01H396IW1WKhfCIKxTy_!!0-item_pic.jpg";
            //req.Ext = null;
            //TbkTpwdCreateResponse rsp = client.Execute(req);
            ////Console.WriteLine(rsp.Body);
            //ViewBag.Body = rsp.Body;
            ITopClient client = new DefaultTopClient(url, appkey, secret);
            TbkTpwdCreateRequest req = new TbkTpwdCreateRequest();
            req.UserId = "123";
            req.Text = "长度大于5个字符";
            req.Url = "https://s.click.taobao.com/t?e=m%3D2%26s%3DoLlSAqt0cH5w4vFB6t2Z2ueEDrYVVa64Dne87AjQPk9yINtkUhsv0NcBLRgUTpxpRyzq1DyPBOW8Xm4%2FuFcvROHroAtoHl6yPesSyNKFeIjFQ3qBgQAZBVMjacS34Cq9I1qc6ZupxAs6ZgOZS7dFueV%2FpTYkWP25iufCphjQGRU89dZ%2FJv6NxrybI%2FWGa7qaUZXZHmVjgVTGJe8N%2FwNpGw%3D%3D&amp;scm=1007.15348.115058.0_28026&amp;pvid=4c710af4-ca53-4611-925b-696f3f2bc20b&amp;app_pvid=59590_11.132.118.125_593_1594450880300&amp;ptl=floorId:28026;originalFloorId:28026;pvid:4c710af4-ca53-4611-925b-696f3f2bc20b;app_pvid:59590_11.132.118.125_593_1594450880300&amp;union_lens=lensId%3AMAPI%401594450880%404c710af4-ca53-4611-925b-696f3f2bc20b_596198137886%401";
            req.Logo = "https://img.alicdn.com/bao/uploaded/i1/2975892770/O1CN01H396IW1WKhfCIKxTy_!!0-item_pic.jpg";
            req.Ext = "{}";
            TbkTpwdCreateResponse rsp = client.Execute(req);
            Console.WriteLine(rsp.Body);
            ViewBag.Body = rsp.Body;
            return View("Index");
        }

        public ActionResult material() {

            ITopClient client = new DefaultTopClient(url, appkey, secret);
            TbkDgOptimusMaterialRequest req = new TbkDgOptimusMaterialRequest();
            req.PageSize = 20L;
            req.AdzoneId = 62416919;
            req.PageNo = 1L;
            req.MaterialId = 28026L;
            req.DeviceValue = null;
            req.DeviceEncrypt = null;
            req.DeviceType = null;
            req.ContentId = null;
            req.ContentSource = null;
            req.ItemId = null; ;
            req.FavoritesId = "";
            TbkDgOptimusMaterialResponse rsp = client.Execute(req);
            //Console.WriteLine(rsp.Body);
            ViewBag.Body = rsp.Body;
            return View("Index");
        }
        /// <summary>
        /// 淘口令转链
        /// </summary>
        /// <returns></returns>
        public ActionResult TbkTpwdConvert() {

            //ITopClient client = new DefaultTopClient(url, appkey, secret);
            //TbkTpwdConvertRequest req = new TbkTpwdConvertRequest();
            //req.PasswordContent = "￥2k12308DjviP￥";
            //req.AdzoneId = 12312312L;
            //req.Dx = "1";
            //TbkTpwdConvertResponse rsp = client.Execute(req);
            ////Console.WriteLine(rsp.Body);
            //ViewBag.Body = rsp.Body;
            return View("Index");
        }


        public ActionResult shopget() {

            ITopClient client = new DefaultTopClient(url, appkey, secret);
            TbkShopGetRequest req = new TbkShopGetRequest();
            req.Fields = "user_id,shop_title,shop_type,seller_nick,pict_url,shop_url";
            req.Q = "女装";
            req.Sort = "total_auction";
            req.IsTmall = false;
            req.StartCredit = 1L;
            req.EndCredit = 20L;
            req.StartCommissionRate = null;
            req.EndCommissionRate = null;
            req.StartTotalAction = null;
            req.EndTotalAction = null;
            req.StartAuctionCount = null;
            req.EndAuctionCount = null;
            req.Platform = 1L;
            req.PageNo = 1L;
            req.PageSize = 20L;
            TbkShopGetResponse rsp = client.Execute(req);
            Console.WriteLine(rsp.Body);
            ViewBag.Body = rsp.Body;
            return View("Index");
        } 
    }
}