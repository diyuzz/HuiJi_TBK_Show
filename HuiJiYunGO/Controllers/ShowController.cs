﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Top.Api;
using Top.Api.Request;
using Top.Api.Response;

namespace HuiJiYunGO.Controllers
{
    public class ShowController : BaseController
    {
       
        // GET: Show
        public ActionResult Index(long page=0)
        {
            if (IsMobile())
            {
                return Redirect("/mobile");
            }

            if (Request.UrlReferrer == null)
            {
                //算一次访问
                AddOpRecord("访问人次", "访问人次", 1);
                if (!IsWeiXin())
                {
                    AddOpRecord("PC", "访问来源", 1);
                }
            }

            var MaterialId = GetAppBarList().Where(m => m.Code == "Index").FirstOrDefault().Material_ID;
            ViewBag.AppBarCode = "Index";
            ViewBag.Page = page;
            ITopClient client = new DefaultTopClient(url, appkey, secret);
            TbkDgOptimusMaterialRequest req = new TbkDgOptimusMaterialRequest();
            req.PageSize = 40L;
            req.AdzoneId = adzoneId;
            req.PageNo = page;
            req.MaterialId = MaterialId;
            req.DeviceValue = null;
            req.DeviceEncrypt = null;
            req.DeviceType = null;
            req.ContentId = null;
            req.ContentSource = null;
            req.ItemId = null; ;
            req.FavoritesId = "";
            TbkDgOptimusMaterialResponse rsp = client.Execute(req);
            //req.PageSize = 8L;
            //req.PageNo = 1L;
            //req.MaterialId = 3756L;
            //TbkDgOptimusMaterialResponse rsp2 = client.Execute(req);
            //req.PageSize = 7L;
            //req.PageNo = 1L;
            //req.MaterialId = 4094L;
            //TbkDgOptimusMaterialResponse rsp3 = client.Execute(req);
            ViewBag.Limit = req.PageSize;
            ViewBag.TotalCount = "500";
            ViewBag.MaterialList = rsp.ResultList;
            //ViewBag.HaoQuan = rsp2.ResultList;
            //ViewBag.TeHui = rsp3.ResultList;

            ViewBag.SlideShowList = GetSlideShowList();
            return View();
        }

        /// <summary>
        /// 移动端
        /// </summary>
        /// <returns></returns>
        public ActionResult Mobile() {

            return View();
        }
    }
}