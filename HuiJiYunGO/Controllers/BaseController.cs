﻿using HuiJiYunGo.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Top.Api;
using Top.Api.Request;
using Top.Api.Response;

namespace HuiJiYunGO.Controllers
{

    public class BaseController : Controller
    {
        public string AdminSite
        {
            get
            {

                return GetSystemParameterList().Find(m => m.Code == "AdminSite").SPValue;

            }
        }
        public string url
        {
            get
            {

                return GetSystemParameterList().Find(m => m.Code == "TBK_API").SPValue;

            }
        }
        public string appkey
        {
            get
            {

                var value = GetAdZone().Find(m => m.IsDefault == true).TBK_AppKey;
                return value;

            }
        }
        public string secret
        {
            get
            {

                var value = GetAdZone().Find(m => m.IsDefault == true).TBK_AppSecret;
                return value;
            }
        }
        public long adzoneId
        {
            get
            {
                var value = GetAdZone().Find(m => m.IsDefault == true).TBK_AdzoneId;
                return (long)value;

            }
        }
        public string Pid
        {
            get
            {
                var value = GetAdZone().Find(m => m.IsDefault == true).Pid;
                return value;

            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HuiJiDBContainer db = new HuiJiDBContainer();
            var AppBar = db.AppBar.Where(m => m.ShowWeb == true).OrderBy(m => m.SortCode).ToList();
            List<AppBarViewModel> AppBarList = new List<AppBarViewModel>();
            foreach (var item in AppBar)
            {
                AppBarList.Add(new AppBarViewModel()
                {
                    Name = item.Name,
                    Link = item.Link_To,
                    Code = item.Code
                });
            }
            ViewBag.AppBarList = AppBarList;
            var AppBarMobile = db.AppBar.Where(m => m.ShowPhone == true).OrderBy(m => m.SortCode).ToList();
            List<AppBarViewModel> AppBarMobileList = new List<AppBarViewModel>();
            foreach (var item in AppBarMobile)
            {
                AppBarMobileList.Add(new AppBarViewModel()
                {
                    Name = item.ShortName,
                    Link = item.LinkMobile_To,
                    Code = item.Code,
                    Icon_B = item.Icon_B,
                    Icon_S = item.Icon_S
                });
            }
            ViewBag.AppBarMobileList = AppBarMobileList;
            List<CategoryLevelOneViewModel> CLVM = new List<CategoryLevelOneViewModel>();
            //取前10个分类
            var Top10CategoryList = db.Category.Where(m => m.Pid == 0 && m.ShowWeb == true).OrderByDescending(m => m.SortCode).Take(8).ToList();
            foreach (var item in Top10CategoryList)
            {
                List<CategoryViewModel> Top5Category2ViewModelList = new List<CategoryViewModel>();
                var Top5Category2List = db.Category.Where(m => m.Pid == item.Id && m.ShowWeb == true).OrderByDescending(m => m.SortCode).Take(3).ToList();
                foreach (var item2 in Top5Category2List)
                {
                    Top5Category2ViewModelList.Add(new CategoryViewModel()
                    {

                        id = item2.Id,
                        name = item2.Name,
                        shortname = item2.ShortName,
                        link = item2.Link_To
                    });
                }
                CLVM.Add(new CategoryLevelOneViewModel()
                {
                    id = item.Id,
                    name = item.Name,
                    shortname = item.ShortName,
                    link = item.Link_To,
                    CategoryList = Top5Category2ViewModelList
                }); ;
            }

            ViewBag.CategoryTop10List = CLVM;


            //加载广告

            var AdvertList = db.Advert.OrderBy(m => m.SortCode).Take(10).ToList();
            ViewBag.AdvertList = AdvertList;

            ViewBag.AdminSite = AdminSite;

            ViewBag.WebModel = GetWebModel();
        }

        protected List<Category> GetCategoryList()
        {

            HuiJiDBContainer db = new HuiJiDBContainer();
            var Category = db.Category.Where(m => !string.IsNullOrEmpty(m.Name)).OrderBy(m => m.SortCode).ToList();
            return Category;
        }

        protected List<AdZone_> GetAdZone()
        {

            HuiJiDBContainer db = new HuiJiDBContainer();
            var adZone = db.AdZone_.OrderBy(m => m.SortCode).ToList();
            return adZone;
        }

        protected List<AppBar> GetAppBarList()
        {

            HuiJiDBContainer db = new HuiJiDBContainer();
            var AppBar = db.AppBar.OrderBy(m => m.SortCode).ToList();
            return AppBar;
        }

        protected List<SystemParameter> GetSystemParameterList()
        {
            HuiJiDBContainer db = new HuiJiDBContainer();
            var SystemParameter = db.SystemParameter.OrderBy(m => m.SortCode).ToList();
            return SystemParameter;
        }

        protected List<SlideShow> GetSlideShowList()
        {
            HuiJiDBContainer db = new HuiJiDBContainer();
            var SlideShow = db.SlideShow.OrderBy(m => m.SortCode).ToList();
            return SlideShow;
        }

        protected List<ItemsDetail> GetMaterialIdList()
        {
            HuiJiDBContainer db = new HuiJiDBContainer();
            var query = from a in db.Items
                        join b in db.ItemsDetail
                        on a.Id equals b.ItemId
                        orderby b.SortCode ascending
                        where a.EnCode == "Material_Id"
                        select b;
            return query.ToList();
        }

        protected string getELELink()
        {

            ITopClient client = new DefaultTopClient(url, appkey, secret);
            TbkActivityInfoGetRequest req = new TbkActivityInfoGetRequest();
            req.AdzoneId = adzoneId;
            req.SubPid = null;
            req.RelationId = null;
            req.ActivityMaterialId = "1571715733668";
            req.UnionId = "huiji";
            TbkActivityInfoGetResponse rsp = client.Execute(req);
            //Console.WriteLine(rsp.Body);
            return rsp.Data.ClickUrl;
        }


        public static bool IsMobile()
        {
            if (System.Web.HttpContext.Current.Request.Browser.IsMobileDevice)
                return true;

            return false;
        }


        public static bool IsWeiXin()
        {
            string userAgent = System.Web.HttpContext.Current.Request.UserAgent;
            if (userAgent.ToLower().Contains("micromessenger"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 是否可转淘口令
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool IsToTKLinkOk(string url)
        {

            if (url.Contains("s.click.taobao.com") || url.Contains("uland.taobao.com"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static void AddOpRecord(string Name, string GName, int OpCount)
        {
            HuiJiDBContainer db = new HuiJiDBContainer();
            var date = DateTime.Now.Date;
            var opRecord = db.OpRecord.FirstOrDefault(m => m.Name == Name && m.GName == GName && m.MarkDate == date);
            if (opRecord != null)
            {

                opRecord.OpCount += OpCount;

                db.Entry(opRecord).State = EntityState.Modified;

                db.Entry(opRecord).Property(x => x.OpCount).IsModified = true;

            }
            else
            {
                db.OpRecord.Add(new OpRecord()
                {
                    GName = GName,
                    Name = Name,
                    OpCount = OpCount,
                    CreateTime = DateTime.Now,
                    CreateUserId = 1,
                    MarkDate = DateTime.Now.Date,
                    SortCode = 0,
                    UpdateTime = DateTime.Now,
                    UpdateUserId = 1

                });

            }
            db.SaveChanges();
        }

        public WebModel GetWebModel()
        {
            var sp = GetSystemParameterList();
            var webModel = new WebModel();
            webModel.SiteName = sp.First(m => m.Code == "SiteName").SPValue;
            webModel.SiteDomain = sp.First(m => m.Code == "SiteDomain").SPValue;
            webModel.Corporate = sp.First(m => m.Code == "CompanyName").SPValue;
            webModel.Address = sp.First(m => m.Code == "CompanyAddress").SPValue;
            webModel.Tel = sp.First(m => m.Code == "CompanyPlane").SPValue;
            webModel.Phone = sp.First(m => m.Code == "CompanyPhone").SPValue;
            webModel.Email = sp.First(m => m.Code == "CompanyEmail").SPValue;
            webModel.HomeTitle = sp.First(m => m.Code == "HomeTitle").SPValue;
            webModel.MetaKey = sp.First(m => m.Code == "METAKey").SPValue;
            webModel.MetaDescribe = sp.First(m => m.Code == "METADes").SPValue;
            webModel.Record = sp.First(m => m.Code == "ICP").SPValue;
            webModel.CopyRight = sp.First(m => m.Code == "COPYRIGHT").SPValue;
            webModel.SiteLogo = sp.First(m => m.Code == "SiteLogo").SPValue;

            return webModel;

        }
    }
}