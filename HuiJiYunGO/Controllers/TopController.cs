﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Top.Api;
using Top.Api.Request;
using Top.Api.Response;

namespace HuiJiYunGO.Controllers
{
    public class TopController : BaseController
    {
        // GET: Top
        // GET: Show
        public ActionResult Index(long page = 1)
        {
            var MaterialId = GetAppBarList().Where(m => m.Code == "TOP").FirstOrDefault().Material_ID;
            ViewBag.AppBarCode = "TOP";
            ViewBag.Page = page;
            ITopClient client = new DefaultTopClient(url, appkey, secret);
            TbkDgOptimusMaterialRequest req = new TbkDgOptimusMaterialRequest();
            req.PageSize = 40L;
            req.AdzoneId = adzoneId;
            req.PageNo = page;
            req.MaterialId = MaterialId;//好券直播 综合 200款商品 
            req.DeviceValue = null;
            req.DeviceEncrypt = null;
            req.DeviceType = null;
            req.ContentId = null;
            req.ContentSource = null;
            req.ItemId = null; ;
            req.FavoritesId = "";
            TbkDgOptimusMaterialResponse rsp = client.Execute(req);
            //req.PageSize = 8L;
            //req.PageNo = 1L;
            //req.MaterialId = 3756L;
            //TbkDgOptimusMaterialResponse rsp2 = client.Execute(req);
            //req.PageSize = 7L;
            //req.PageNo = 1L;
            //req.MaterialId = 4094L;
            //TbkDgOptimusMaterialResponse rsp3 = client.Execute(req);
            ViewBag.Limit = req.PageSize;
            ViewBag.TotalCount = "200";
            ViewBag.MaterialList = rsp.ResultList;
            //ViewBag.HaoQuan = rsp2.ResultList;
            //ViewBag.TeHui = rsp3.ResultList;
            return View();
        }
    }
}