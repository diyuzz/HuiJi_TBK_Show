﻿using HuiJiYunGo.EF;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Top.Api;
using Top.Api.Request;
using Top.Api.Response;

namespace HuiJiYunGO.Controllers
{
    public class RushController : BaseController
    {
        // GET: Rush
        public ActionResult Index()
        {
            //// 0 8 10 13 15 17 19 22 0
            //var MaterialId = GetAppBarList().Where(m => m.Code == "Rush").FirstOrDefault().Material_ID;
            ViewBag.AppBarCode = "Rush";

            //ITopClient client = new DefaultTopClient(url, appkey, secret);
            //TbkJuTqgGetRequest req = new TbkJuTqgGetRequest();
            //req.AdzoneId = adzoneId;
            //req.Fields = "click_url,pic_url,reserve_price,zk_final_price,total_amount,sold_num,title,category_name,start_time,end_time";
            //req.StartTime = DateTime.Parse("2020-07-16 00:00:00");
            //req.EndTime = DateTime.Parse("2020-07-16 23:59:59");
            //req.PageNo = 1L;
            //req.PageSize = 40L;
            //TbkJuTqgGetResponse rsp = client.Execute(req);
            //Console.WriteLine(rsp.Body);
            //var ret = rsp.Results;
            ////ViewBag.Body = Newtonsoft.Json.JsonConvert.SerializeObject(ret);

            //ViewBag.TbkJuTqg = rsp.Results;
            //ViewBag.GroupTimeString = string.Join(",",rsp.Results.GroupBy(m=>m.StartTime).Select(m=>m.Key).ToList());

            DateTime now = DateTime.Now;
            DateTime todayStart = new DateTime(now.Year, now.Month, now.Day);//今日开始
            DateTime tomorrowEnd = todayStart.AddDays(2).AddSeconds(-1); ;//明日结束
            HuiJiDBContainer db = new HuiJiDBContainer();
            var RushList=db.Rush.Where(m=>m.StartTime>= todayStart&&m.StartTime<= tomorrowEnd).ToList();//拿到所有抢购信息

            List<DateTime?> TimeGroup=RushList.GroupBy(m => m.StartTime).Select(m=>m.Key).ToList();
            List<RushViewModel> RvwList = new List<RushViewModel>();
            for (int i = 0; i < TimeGroup.Count; i++)
            {
                RushViewModel rvw = new RushViewModel();
                rvw.Rushs = RushList.Where(m => m.StartTime == TimeGroup[i]).ToList();
                rvw.StartTime = TimeGroup[i].Value;
                rvw.TimeStr = (TimeGroup[i].Value.Date==DateTime.Now.Date?"今":"明")+ TimeGroup[i].Value.ToString("t");
                if(i != TimeGroup.Count-1&&DateTime.Now > TimeGroup[i]&& DateTime.Now < TimeGroup[i+1])
                rvw.IsCurrent = true;
                RvwList.Add(rvw);
            }
            //foreach (var item in TimeGroup)
            //{
            //    RushViewModel rvw = new RushViewModel();
            //    rvw.Rushs = RushList.Where(m => m.CreateTime == item).ToList();
            //    rvw.StartTime = item.Value;
            //    rvw.TimeStr = item.Value.ToString("t");

            //    rvw.IsCurrent = false;
            //}

            ViewBag.RushData = RvwList;
            return View();
        }
    }
}