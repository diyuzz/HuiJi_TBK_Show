﻿using HuiJiYunGo.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Top.Api;
using Top.Api.Request;
using Top.Api.Response;

namespace HuiJiYunGO.Controllers
{
    public class NPNController : BaseController
    {
        // GET: NPN
        /// <summary>
        /// 好物精选-商品搜索
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        // GET: Search
        public ActionResult Index(string Key = "", int Page = 1, int? ep = null, int? sp = null, int mid = 0, string sort = "")
        {
            var MaterialId = GetAppBarList().Where(m => m.Code == "NPN").FirstOrDefault().Material_ID;//NPN 默认物料
            var MaterialIdList = GetMaterialIdList();//字典里面的物料
            if (mid==0)
            {
                mid = MaterialId.Value;
            }
            ViewBag.MaterialIdList = MaterialIdList;
            ViewBag.AppBarCode = "NPN";
            ViewBag.Key = Key;
            ViewBag.Page = Page;
            if (string.IsNullOrWhiteSpace(sort))
            {
                sort = "tk_total_sales_des";
            }
            ViewBag.Sort = sort;
            ViewBag.Mid = mid;
            ITopClient client = new DefaultTopClient(url, appkey, secret);
            TbkDgMaterialOptionalRequest req = new TbkDgMaterialOptionalRequest();
            req.StartDsr = null;
            req.PageSize = 15;
            req.PageNo = Page;
            req.Platform = 1L;
            req.EndTkRate = null;
            req.StartTkRate = null;
            req.EndPrice = 20;
            req.StartPrice = 0;
            req.IsOverseas = false;
            req.IsTmall = false;
            req.Sort = sort;
            req.Itemloc = null;
            req.Cat = null;
            req.Q = Key;
            req.MaterialId = mid;
            req.HasCoupon = true;
            req.Ip = null;
            req.AdzoneId = adzoneId;
            req.NeedFreeShipment = true;
            req.NeedPrepay = true;
            req.IncludePayRate30 = true;
            req.IncludeGoodRate = true;
            req.IncludeRfdRate = true;
            req.NpxLevel = 2L;
            req.EndKaTkRate = null;
            req.StartKaTkRate = null;
            req.DeviceEncrypt = null;
            req.DeviceValue = null;
            req.DeviceType = null;
            req.LockRateEndTime = null;
            req.LockRateStartTime = null;
            req.Longitude = null;
            req.Latitude = null;
            req.CityCode = null;
            req.SellerIds = null;
            req.SpecialId = null;
            req.RelationId = null;
            TbkDgMaterialOptionalResponse rsp = client.Execute(req);
            if (rsp.IsError)
            {
                //失败就在来一次
                rsp = client.Execute(req);
            }
            //Console.WriteLine(rsp.Body);
            ViewBag.TotalResults = 200;//rsp.TotalResults;
            ViewBag.ResultList = rsp.ResultList;




            return View();
        }
    }
}