﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuiJiYunGO
{
    public class WebModel
    {/// <summary>
     /// 网站名称
     /// </summary>
        public string SiteName { get; set; }
        /// <summary>
        /// 网站域名
        /// </summary>
        public string SiteDomain { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Corporate { get; set; }
        /// <summary>
        /// 公司地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 公司座机
        /// </summary>
        public string Tel { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 电子邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 首页标题
        /// </summary>
        public string HomeTitle { get; set; }
        /// <summary>
        /// META关键词:多个关键词用英文状态 , 号分割
        /// </summary>
        public string MetaKey { get; set; }
        /// <summary>
        /// META描述
        /// </summary>
        public string MetaDescribe { get; set; }
        /// <summary>
        /// 网站备案号
        /// </summary>
        public string Record { get; set; }
        /// <summary>
        /// 版权信息
        /// </summary>
        public string CopyRight { get; set; }

        /// <summary>
        /// 网站logo
        /// </summary>
        public string SiteLogo { get; set; }
    }
}