﻿using HuiJiYunGo.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuiJiYunGO
{
    public class RushViewModel
    {
        /// <summary>
        /// 是否在当前时间段
        /// </summary>
        public bool IsCurrent { get; set; }
        public string TimeStr { get; set; }
        public DateTime StartTime { get; set; }
        public List<Rush> Rushs { get; set; }
    }
}