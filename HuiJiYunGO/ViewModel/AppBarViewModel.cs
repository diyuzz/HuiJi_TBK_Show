﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuiJiYunGO
{
    public class AppBarViewModel
    {
        public string Name { get; set; }
        public string Link { get; set; }
        public string Code { get; set; }

        public string Icon_B { get; set; }
        public string Icon_S { get; set; }
    }
}