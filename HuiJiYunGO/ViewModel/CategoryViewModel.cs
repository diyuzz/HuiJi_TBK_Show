﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuiJiYunGO
{
    public class CategoryLevelOneViewModel
    {
        public string name { get; set; }

        public string shortname { get; set; }

        public long id { get; set; }

        public string  al_id { get; set; }

        public string link { get; set; }
        public List<CategoryViewModel> CategoryList { get; set; }
    }
    public class CategoryViewModel
    {
        public string name { get; set; }

        public string shortname { get; set; }

        public long id { get; set; }

        public string al_id { get; set; }

        public string link { get; set; }
    }
}