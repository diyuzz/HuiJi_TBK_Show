# HuiJi_TBK_Show

#### 介绍

惠集（灰机）淘宝客 CMS 程序，一次部署全自动采集商品数据，集成了淘抢购，物料精选，商品搜索，相关推荐，淘口令生成等功能，程序分为两部分，一是展示站 二是管理站，主要采用的技术是 .net MVC SQL Server LayUI Quartz(定时器) 注：程序基于 ok-admin 和 FineAdmin.Mvc 两个开源项目进行的二次开发 表示感谢

此为展示站，需配合Admin程序运行



#### 安装教程

1.  搭建 HuiJi_TBK_Admin 管理站
2.  搭建 HuiJi_TBK_Show  演示站
3.  访问即可

#### 项目地址 （花生壳）有时不能打开，请联系作者 qq：1055548738

[演示地址](http://huiji.ones.net.cn)  
http://huiji.ones.net.cn 演示站 支持移动端和WEB端 支持微信打开 

#### 使用教程
[惠集（灰机）淘客CMS 建站教程](https://www.cnblogs.com/diyuzz/p/13457182.html)
https://www.cnblogs.com/diyuzz/p/13457182.html

#### 项目截图
![输入图片说明](https://gitee.com/diyuzz/HuiJi_TBK_Admin/raw/master/项目说明/Mobile99.png "在这里输入图片标题")

![输入图片说明](https://gitee.com/diyuzz/HuiJi_TBK_Admin/raw/master/项目说明/WebIndex.png "在这里输入图片标题")

![输入图片说明](https://gitee.com/diyuzz/HuiJi_TBK_Admin/raw/master/项目说明/webAppBar.png "在这里输入图片标题")