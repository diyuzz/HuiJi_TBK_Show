//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HuiJiYunGo.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class MaterialGoods
    {
        public int Id { get; set; }
        public Nullable<int> Mid { get; set; }
        public Nullable<int> Material_id { get; set; }
        public Nullable<System.DateTime> OverTime { get; set; }
        public Nullable<int> SortCode { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public Nullable<int> CreateUserId { get; set; }
        public Nullable<System.DateTime> UpdateTime { get; set; }
        public Nullable<int> UpdateUserId { get; set; }
        public Nullable<int> TkTotalSales { get; set; }
        public string CouponId { get; set; }
        public string ItemId { get; set; }
        public string Title { get; set; }
        public string PictUrl { get; set; }
        public string SmallImages { get; set; }
        public string ReservePrice { get; set; }
        public Nullable<decimal> ZkFinalPrice { get; set; }
        public Nullable<int> UserType { get; set; }
        public string Provcity { get; set; }
        public string ItemUrl { get; set; }
        public string CommissionRate { get; set; }
        public Nullable<int> Volume { get; set; }
        public string SellerId { get; set; }
        public Nullable<int> CouponTotalCount { get; set; }
        public Nullable<int> CouponRemainCount { get; set; }
        public string CouponInfo { get; set; }
        public string CommissionType { get; set; }
        public string ShopTitle { get; set; }
        public Nullable<int> ShopDsr { get; set; }
        public string CouponShareUrl { get; set; }
        public string ClickUrl { get; set; }
        public string LevelOneCategoryName { get; set; }
        public Nullable<int> LevelOneCategoryId { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public string ShortTitle { get; set; }
        public string WhiteImage { get; set; }
        public string CouponStartFee { get; set; }
        public Nullable<int> CouponAmount { get; set; }
        public string ItemDescription { get; set; }
        public string Nick { get; set; }
        public string SalePrice { get; set; }
        public Nullable<long> TmallPlayActivityEndTime { get; set; }
        public Nullable<long> TmallPlayActivityStartTime { get; set; }
        public string CouponStartTime { get; set; }
        public string WordList { get; set; }
        public string FavoritesInfo { get; set; }
    }
}
