
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/07/2020 08:49:06
-- Generated from EDMX file: D:\NetApp\HuiJi\HuiJiYunGo.EF\HuiJiDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HuiJiDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[SystemSettingSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SystemSettingSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'SystemSettingSet'
CREATE TABLE [dbo].[SystemSettingSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [AppId] nvarchar(max)  NOT NULL,
    [AppSecret] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'SystemSettingSet'
ALTER TABLE [dbo].[SystemSettingSet]
ADD CONSTRAINT [PK_SystemSettingSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------